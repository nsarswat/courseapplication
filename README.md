# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

	Eclipse IDE
	MySQL server

* Configuration

	egit plugin insallation for commit in bitbucket

* Dependencies

	Spring MVC,
	JDBC,
	MYSQL Server,
	JACKSON e.tc

* Database configuration

	MYSQL Server

* How to run tests

	create student table then run Junit test ---In progress

* Deployment instructions

	deployed and test on tomcat 7
	http://localhost:8080/courseApp/course/create -- to add course
	http://localhost:8080/courseApp/courses -- to view courses
	http://localhost:8080/courseApp/course/delete/courseId -- to delete course
	http://localhost:8080/courseApp/universities -- to view universities using rest service


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact